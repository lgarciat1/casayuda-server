<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="./static/css/Pantallas_estilos.css" type="text/css">
    <style>
        /**
        * Estilo CSS de la tabla.
        * Si lo pongo en el archivo de estilos no funciona.
         */
        @import url('https://fonts.googleapis.com/css2?family=Abril+Fatface&family=Roboto:ital,wght@0,100;0,300;1,100;1,300&display=swap');

        div.contenedorLogin {
            margin: 10em auto;
            border: 1px solid black;
            border-radius: 10px;
            height: 100px;
            width: 600px;
            background-color: #ededed;
            display: flex;
            justify-content: center;
            align-items: center;
        }

        div.contenedor {
            margin: 0 auto;
            border: 1px solid black;
            border-radius: 10px;
            height: 60px;
            width: 600px;
            background-color: #ededed;
            display: flex;
            justify-content: center;
            align-items: center;
        }

        .tabla tbody tr {
            cursor: pointer;
        }
    </style>
</head>

<body>
    <?php
    session_start();
    /* 
        Compruebo si el usuario está logueado. Si no lo esta lo envió a una página para que se pueda loguear.
        Y si el usuario está logueado lo llevo a la pantalla principal.
    */
    if (empty($_SESSION['user_id'])) {
    ?>
        <div class="header">
            <p class="logo"><a class="logomain" href="/main.php">Casayuda</a></p>
        </div>
        <div class="contenedorLogin">
            <p>No estás logueado, para poder logearse pulse <a href='/login.php'>aquí</a>.</p>
        </div>

    <?php
    } else {
    ?>
        <div class="header">
            <p class="logo"><a class="logomain" href="/main.php">Casayuda</a></p>
            <div class="header-right">
                <ul>
                    <li>
                        <!-- Creo el formulario que consta de un placeholder el cuál ejecuta un método GET -->
                        <form method="GET">
                            <input id=buscador name="buscador" type="text" placeholder="Buscar..."></input>
                            <button type="submit"><i class="fa fa-search"></i></button>
                        </form>
                    </li>
                    <!-- Botón para ir a mis contratos -->
                    <li class="misContratos"><a href="/my_hirings.php">Mis Contratos</a></li>
                    <!-- Este sería el botón para poder cerrar la sesión el cuál te lleva a la página do_logout.php -->
                    <li class="cerrarSesion"><a href="/do_logout.php">Cerrar Sesión</a></li>
                </ul>
            </div>
        </div>
        <div>
            <table class="tabla">
                <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Apellidos</th>
                        <th>Trabajo</th>
                        <th>Precio/Hora</th>
                    </tr>
                </thead>
                <tbody>
                    <?php

                    // Indicamos que se require el siguiente fichero
                    require __DIR__ . '/../php_util/db_connection.php';

                    $mysqli = get_db_connection_or_die();

                    // Si el buscador que viene por el GET no esta vacío pues se devuelven las filas que coincidan con el buscador
                    if (!empty($_GET['buscador'])) {
                        $buscar = $_GET['buscador'];
                        $sql = "SELECT id, name, surname, jobs, price_per_hour FROM tUser where jobs IS NOT NULL AND (UPPER(name) LIKE UPPER(?) OR UPPER(surname) LIKE UPPER(?)) ORDER BY id LIMIT 10";
                        $stmt = $mysqli->prepare($sql);
                        $stmt->bind_param("ss", $buscar, $buscar);
                        $stmt->execute();
                        $resultado = $stmt->get_result();
                        // Si el buscador que viene por el GET esta vacío pues se devuelven las 10 primeras filas
                    } else {
                        $sql = "SELECT id, name, surname, jobs, price_per_hour FROM tUser where jobs IS NOT NULL ORDER BY id LIMIT 10";
                        $stmt = $mysqli->prepare($sql);
                        $stmt->execute();
                        $resultado = $stmt->get_result();
                    }

                    // Si no se encuentra a ningún trabajador se devuelve un mensaje
                    if (mysqli_num_rows($resultado) == 0) {
                    ?>
                        <tr>
                            <td colspan="4" style="text-align: center">No existe ninguna oferta registrada.</td>
                        </tr>
                        <?php
                    } else {
                        // Con el siguiente while se autocompleta la tabla con los datos recibidos
                        while ($fila = $resultado->fetch_array(MYSQLI_NUM)) {
                        ?>
                            <tr onclick="window.location='/worker_detail.php?id=<?php echo $fila[0] ?>';">
                                <td><?php echo $fila[1] ?></td>
                                <td><?php echo $fila[2] ?></td>
                                <td><?php echo $fila[3] ?></td>
                                <td><?php echo $fila[4] ?></td>
                            </tr>
                    <?php
                        }
                    }
                    ?>
                </tbody>
            </table>
        </div>
        <?php
        if (mysqli_num_rows($resultado) == 10) {
        ?>
            <div class="contenedor">
                <p>Se están mostrando las diez primeras filas</p>
            </div>
    <?php
        }
    }

    $stmt->close();
    mysqli_close($mysqli);
    ?>
</body>

</html>
