<?php
    require __DIR__ . '/../php_util/db_connection.php';

    $mysqli = get_db_connection_or_die()
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ofertas de empleo</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="./static/css/Pantallas_estilos.css">
</head>
<style>
    @import url('https://fonts.googleapis.com/css2?family=Abril+Fatface&family=Roboto:ital,wght@0,100;0,300;1,100;1,300&display=swap');
    div.contenedor {
        margin: 10em auto;
        border: 1px solid black;
        border-radius: 10px;
        height: 200px;
        width: 600px;
        background-color: #e3e3e3;
        display: flex;
        justify-content: center;
        align-items: center;
    }
</style>
<body>
    <?php
        session_start();
        
        //Respuesta para cuando no existe ningún valor en $_SESSION
        if (empty($_SESSION['user_id'])) {
            echo '<div class="header">';
                echo '<p class="logo">Casayuda</p>';
            echo '</div>';
            echo '<div class="contenedor">';
                echo '<p>El trabajdor no está logueado, para poder logearse pulse <a href="/login.php">aquí</a>.</p>';
            echo '</div>';

        //Respuestá para cuando sí se detecta un id de usuario en $_SESSION
        } else {
            $user_id = $_SESSION['user_id'];
            //Query para saber si el id del usuario detectado pertenece o no a un trabajador.
            $query = "SELECT jobs FROM tUser WHERE id = '".$user_id."'";
            $comprobacion = mysqli_query($mysqli, $query) or die('Query error');
            $only_row = mysqli_fetch_array($comprobacion);
            //Respuesta si el id pertenece a un trabajador:
            if ($only_row[0] != NULL) {
                echo '<div class="header">';
                    echo '<p>Casa Ayuda</p>';
                    echo '<div class="header-right">';
                        echo '<ul>';
                        echo '<li>';
                            //Implementado el buscador (propuesta de Francisco Caneda)
                            echo '<form method="GET">';
                                echo '<input id=buscador name="buscador" type="text" placeholder="Buscar..."></input>';
                                echo '<button type="submit"><i class="fa fa-search"></i></button>';
                            echo '</form>';
                        echo '</li>';
                        echo '<li class="cerrarSesion"><a href="/do_logout.php">Cerrar Sesión</a></li>';
                        echo '</ul>';
                    echo '</div>';
                echo '</div>';
                echo '<h1 style="text-align: center; margin-top: 50px;">Tus ofertas</h1>';
                echo '<table>';
                    echo '<tr>';
                        echo '<th>Nombre del cliente</th>';
                        echo '<th>Fecha</th>';
                        echo '<th>Mensaje</th>';
                        echo '<th>Estado</th>';
                    echo '</tr>';
                    //Implementada la query si detecta un valor dentro del GET extraido del formulario de la busqueda.
                    if (!empty($_GET['buscador'])) {
                        $buscar = $_GET['buscador'];
                        //Resultado según el nombre de usuario. Se usa MySQLi Prepared Statements
                        $query = "SELECT * FROM tRequest JOIN tUser ON tUser.id = tRequest.requester_user_id WHERE worker_user_id = '".$user_id."' AND (UPPER(name) LIKE UPPER(?)) LIMIT 10";
                        $stmt = $mysqli->prepare($query);
                        $stmt->bind_param("s", $buscar);
                        $stmt->execute();
                    } else {
                        //Resultado que devuelve toda las tuplas de la base de datos.
                        $query = "SELECT * FROM tRequest JOIN tUser ON tUser.id = tRequest.requester_user_id WHERE worker_user_id = '".$user_id."' LIMIT 10";
                        $stmt = $mysqli->prepare($query);
                        $stmt->execute();
                    }
                    $resultado = $stmt->get_result();
                    //Respuesta si no se detecta ninguna coincidencia entre el id del trabajador dentro de la tabla tRequest
                    if (mysqli_num_rows($resultado)==0) {
                        echo '<tr>';
                        echo '<td colspan="4" style="text-align: center">No existe ninguna oferta registrada.</td>';
                        echo '</tr>';   
                    }
                    //Si se encuentra coincidencia, las imprimirá en forma de tabla
                    while ($row = mysqli_fetch_array($resultado)) {
                        //ENlace de los detalles de cada oferta al pulsar en cada fila
                        ?><tr onclick="window.location='/offer.php?id=<?php echo $row['id'] ?>';"><?php
                            echo '<td>'.$row['name'].'</td>'; 
                            echo '<td>'.$row['datetime'].'</td>';
                            echo '<td>'.$row['message'].'</td>'; 
                            if ($row['is_accepted']==1) {
                                echo '<td>Aceptado</td>';
                            } else {
                                echo '<td>No aceptado</td>';
                            }
                        echo '</tr>';
                    }
                echo '</table>';
                if(mysqli_num_rows($resultado) == 10) {
                    echo '<div class="contenedor">';
                        echo '<p>Se están mostrando las diez primeras filas</p>';
                    echo '</div>';
                }
                $stmt->close();
                mysqli_close($mysqli);
            } else {
                //Si resulta que no es un trabajador, no se le dejará acceder a la tabla.
                echo '<div class="header">';
                    echo '<p>Casa Ayuda</p>';
                echo '</div>';
                echo '<div class="contenedor">No se puede acceder a esta información si no eres un trabajador.</div>';
            }
                        
        }
    ?>
</body>
</html>
