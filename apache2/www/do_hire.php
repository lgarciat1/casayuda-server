<?php
    require __DIR__ . '/../php_util/db_connection.php';
    //conexión a la base de datos
    $mysqli = get_db_connection_or_die();
    session_start();

    //recuperar los valores del inicio de sesión
    $requester_user_id = $_SESSION['user_id'] ;

    //capturo los valores que vienen del formulario
    $date = $_POST['date'];
    $message = $_POST['descripcion'];
    $worker_user_id = $_POST['worker_id'] ;

    //Crear una nueva request en la base de datos
    try{
        $sql = "INSERT INTO tRequest (datetime, message, requester_user_id, worker_user_id) VALUES(?,?,?,?)";
        $stmt = $mysqli -> prepare($sql);
        $stmt -> bind_param("ssii", $date, $message, $requester_user_id, $worker_user_id);
        $stmt -> execute();

     if (!empty($mysqli->error)){
            exit();
    }
    $stmt -> close();
    } catch(Exception $e){
    exit();
     }

// Tras crear una el insert , nos redirige a la página main
    header('Location: main.php');
    
?>

