<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="./static/css/Pantallas_estilos.css" type="text/css">
    <link rel="stylesheet" href="./static/css/hire_css.css" type="text/css">
    <title>Document</title>

</head>
<body>
    
<?php
    session_start();
    /* 
        Compruebo si el usuario está logueado. Si no lo esta lo envió a una página para que se pueda loguear.
        Y si el usuario está logueado lo llevo a la pantalla principal.
    */
    $userid = $_SESSION['user_id'];
    if (empty($userid)) {
    ?>
        <div class="header">
            <p class="logo"><a class="logomain" href="/main.php">Casayuda</a></p>

        </div>
        <div id="contenedorLogin">
            <p>No estás logueado, para poder logearse pulse <a href='/login.php'>aquí</a>.</p>
        </div>

    <?php

    } else {
        // conexión a la base de datos 
        require __DIR__ . '/../php_util/db_connection.php';
        //recuperamos el worker_id 
        $worker_id = $_GET['worker_id'];
        $mysqli = get_db_connection_or_die();
        //preparamos una consulta a la base de datos que nos recuperara el nombre del trabajador correspondiente con el id de arriba
        $sql = 'SELECT name FROM tUser WHERE id = ?';
        $stmt = $mysqli->prepare($sql);
        $stmt->bind_param("i", $worker_id );
        $stmt->execute();
        $stmt -> store_result();
        // Si no se encuentra a ningún trabajador se devuelve un mensaje
        if ($stmt -> num_rows === 0 ) {
            echo 'esto falla';
        //Si se encuentra se encuentra el nombre del trabajador, se almacena una una variable y se cierra la conexión con la base de datos
        }else{
            $stmt -> bind_result($name);
            $stmt -> fetch();
            $Tnombre = $name;
            $stmt -> close();
        }
        }

     ?>
     <!-- Formulario donde escribiremos los datos, consta de una etiqueta <p> donde mostraremos el  nombre del trabajador a contratar, un par de label ; y 3 input : uno "type=date" para la fecha , un segundo siendo un "textarea" para la descripción y un ultimo oculto para poder enviar el id del trabajador en el formulario-->
    
     <div id="formulario">
            <form action="do_hire.php" method="post">
            <p> Contratar a <?php echo $Tnombre ?> </p>

            <label for="date"> Fecha para la contratación</label>
            <input type="date" name="date" id="fecha" placeholder="Fecha para la contratación">

            <label for="descripcion"> Descripición: </label>
            <textarea name="descripcion" id="descripcion" placeholder=" Introduce Una breve descripcion para el trabajador sobre el trabajo que quieres que realice " cols="40" rows="15"></textarea>

            <input id="prodId" name="worker_id" type="hidden" value="<?php echo $worker_id?>">
            <button type="submit" id="button" > Crear Petición</button>
        </form>
        </div>
    
    </body>
</html>
