<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="./static/css/Pantallas_estilos.css" type="text/css">
    <style>
        /**
        * Estilo CSS de la tabla.
        * Si lo pongo en el archivo de estilos no funciona.
         */
        @import url('https://fonts.googleapis.com/css2?family=Abril+Fatface&family=Roboto:ital,wght@0,100;0,300;1,100;1,300&display=swap');

        div.contenedorLogin {
            margin: 10em auto;
            border: 1px solid black;
            border-radius: 10px;
            height: 100px;
            width: 600px;
            background-color: #ededed;
            display: flex;
            justify-content: center;
            align-items: center;
        }

        div.contenedor {
            margin: 0 auto;
            border: 1px solid black;
            border-radius: 10px;
            height: 60px;
            width: 600px;
            background-color: #ededed;
            display: flex;
            justify-content: center;
            align-items: center;
        }

        .tabla tbody tr {
            cursor: pointer;
        }
    </style>
</head>

<body>
    <?php
    session_start();
    /* 
        Compruebo si el usuario está logueado. Si no lo esta lo envió a una página para que se pueda loguear.
        Y si el usuario está logueado lo llevo a la pantalla principal.
    */
    if (empty($_SESSION['user_id'])) {
    ?>
        <div class="header">
            <p class="logo"><a class="logomain" href="/main.php">Casayuda</a></p>

        </div>
        <div class="contenedorLogin">
            <p>No estás logueado, para poder logearse pulse <a href='/login.php'>aquí</a>.</p>
        </div>

    <?php
    } else {
    ?>
        <div class="header">
            <p class="logo"><a class="logomain" href="/main.php">Casayuda</a></p>
            <div class="header-right">
                <ul>
                    <!-- Botón para ir a mis contratos -->
                    <li class="misContratos"><a href="/my_hirings.php">Mis Contratos</a></li>
                    <!-- Este sería el botón para poder cerrar la sesión el cuál te lleva a la página do_logout.php -->
                    <li class="cerrarSesion"><a href="/do_logout.php">Cerrar Sesión</a></li>
                </ul>
            </div>
        </div>
        <div>
            
                    <?php

                    // Indicamos que se require el siguiente fichero
                    require __DIR__ . '/../php_util/db_connection.php';
                    // Conectamos con la base de datos
                    $mysqli = get_db_connection_or_die();
                    //Preparamos la sentencia
                    $sql = "SELECT tRequest.id, tRequest.is_accepted, tRequest.datetime, tRequest.message, tUser.name, tUser.jobs FROM tRequest INNER JOIN tUser ON tUser.id = tRequest.worker_user_id WHERE tRequest.requester_user_id = (?) ORDER BY tRequest.datetime";
                    $stmt = $mysqli->prepare($sql);
                    $stmt->bind_param("i", $_SESSION['user_id']);
                    $stmt->execute();
                    $resultado = $stmt->get_result();
                    // Si no se encuentra a ningún trabajador se devuelve un mensaje
                    if (mysqli_num_rows($resultado) == 0) {
                    ?>
                        <div class="contenedorLogin">
                            <p>No existen contrataciones hechas por el usuario.</p>
                        </div>
                        <?php
                    } else {
                        ?>
                        <table class="tabla">
                        <caption><h1>Mis Contratos</h1></caption>
                        <thead>
                            <tr>
                                <!-- Creamos la tabla con sus cabeceras -->
                                <th>Estado</th>
                                <th>Fecha</th>
                                <th>Mensaje</th>
                                <th>Trabajador</th>
                                <th>Trabajos</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                        // Con el siguiente while se autocompleta la tabla con los datos recibidos
                        while ($fila = $resultado->fetch_array(MYSQLI_NUM)) {
                        ?>
                            <!-- <tr onclick="window.location='/offer.php?id=<?php 
                            //echo $fila[0] ?>';"> -->
                            <tr>
                                <?php
                                $fechanow = new DateTime('NOW');
                                $fechanow =   $fechanow->format('Y-m-d H:i:s');
                                // En función del estado de la contratación pintamos la columna del estado de un color u otro
                                if ($fila[1]) {
                                ?>
                                    <td style="color:forestgreen">Aceptada</td>
                                <?php
                                } else {
                                ?>
                                    <td style="color:darkorange">Pendiente</td>
                                <?php
                                }
                                // En función de la fecha pintamos la columna de fecha de un color u otro
                                if ($fechanow < $fila[2]) {
                                ?>
                                    <td style="color:green"><?php echo $fila[2] ?> Activa</td>
                                <?php
                                } else {
                                ?>
                                    <td style="color:red"><?php echo $fila[2] ?> Terminada</td>
                                <?php
                                }
                                ?>
                                <!-- Imprimimos el resto de valores. -->
                                <td><?php echo $fila[3] ?></td>
                                <td><?php echo $fila[4] ?></td>
                                <td><?php echo $fila[5] ?></td>
                            </tr>
                    <?php
                        }
                    }
                    ?>
                </tbody>
            </table>
        </div>

    <?php
    }
    $stmt->close();
    mysqli_close($mysqli);
    ?>
</body>

</html>
