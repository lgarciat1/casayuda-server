<!-- En estas linea de PHP lo que hacemos es llamar a una funcion que nos ayuda -->
<!-- en la conexión a la base de datos. -->
<?php
    require __DIR__. '/../php_util/db_connection.php';
?>


<?php
	// Creamos la variable de la conexión a la base de datos. 
	// LLamando al metodo que hay en el fichero mencionado arriba
    $db = get_db_connection_or_die();

	// Guardamos en variables. Los parametros pasado por el Login.php
	$email_p = $_POST['email_p'];
	$pass_p = $_POST['pass_p'];

	//Aqui creamos la variable con la consulta a la base de datos. 
	// Lamando al id, la contraseña y los trabajos. De ese email
	$query = "SELECT id, encrypted_password, jobs FROM tUser WHERE email = '".$email_p."'";
	// Hacemos la consulta a la base de datos con es Query y si da error nos redirige al login.php?login_failed_unknown=True.
	$result = mysqli_query($db, $query) or die(header('Location: login.php?login_failed_unknown=True'));
	// Si entra en el if es que el email está correcto
	if (mysqli_num_rows($result) > 0) {
		// Convierte en array el resultado de la consulta
		$only_row = mysqli_fetch_array($result);
		// Comparamos la contraseña cifrada, con la contraseña que pasó por parametro en el login.php
		if (password_verify($pass_p,$only_row[1])) {
			// Si está correcta inicia sesion y guarda el user ID
			session_start();
			$_SESSION['user_id'] = $only_row[0];
			// No tiene trabajos, nos redirige a main.php.
			if ($only_row[2] == NULL) {
				header('Location: main.php');
				// SI tiene trabajos, nos redirige a worker.php.
			}else{
				header('Location: worker.php');
			}
			// Si la contraseña falló nos redirige a login.php?login_failed_password=True
		} else {
			header('Location: login.php?login_failed_password=True');
		}
		// Si el email falló nos redirege a login.php?login_failed_email=True
	} else {
		header('Location: login.php?login_failed_email=True');
	}
?>