document.addEventListener("DOMContentLoaded", function() {
document.getElementById("login").addEventListener('submit', validarFormulario); 
  });

  function validarFormulario(event) {
    event.preventDefault();
     var email_p = document.getElementById('email_p').value;
      if(email_p.length == 0) {
      alert("No has escrito nada en el campo del email.");
      return;
    }
    var pass_p = document.getElementById('pass_p').value;
    if (pass_p.length == 0) {
      alert('No has puesto nada en el campo de la contraseña.');
      return;
    }
    this.submit();
  }