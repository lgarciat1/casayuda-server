CREATE DATABASE IF NOT EXISTS casayudadb;

USE casayudadb;

CREATE TABLE tUser (
  id INTEGER PRIMARY KEY AUTO_INCREMENT,
  name VARCHAR(50) NOT NULL,
  surname VARCHAR(100) NOT NULL,
  email VARCHAR(200) NOT NULL UNIQUE,
  encrypted_password VARCHAR(100) NOT NULL,
  jobs VARCHAR(200),
  price_per_hour INTEGER,
  latitude DECIMAL(8,6),
  longitude DECIMAL(9,6),
  active_session_token CHAR(20)
);

CREATE TABLE tRequest (
  id INTEGER PRIMARY KEY AUTO_INCREMENT,
  datetime DATETIME NOT NULL,
  message VARCHAR(200) NOT NULL,
  requester_user_id INTEGER NOT NULL,
  worker_user_id INTEGER NOT NULL,
  is_accepted BOOLEAN NOT NULL DEFAULT FALSE,
  
  FOREIGN KEY (requester_user_id) REFERENCES tUser(id),
  FOREIGN KEY (worker_user_id) REFERENCES tUser(id)
);

CREATE TABLE tComment (
  id INTEGER PRIMARY KEY AUTO_INCREMENT,
  author_user_id INTEGER NOT NULL,
  worker_user_id INTEGER NOT NULL,
  message VARCHAR(200),
  rating INTEGER NOT NULL,
  
  FOREIGN KEY (author_user_id) REFERENCES tUser(id),
  FOREIGN KEY (worker_user_id) REFERENCES tUser(id)
);
