"""casayuda URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from restapi import endpoints_user, endpoints_workers, endpoints_requests, endpoints_feedback

urlpatterns = [
    path('admin/', admin.site.urls),
    path('rest/version/1/users',endpoints_user.register),
    path('rest/version/1/sessions',endpoints_user.session),
    path('rest/version/1/workers', endpoints_workers.workers),
    path('rest/version/1/workers/<int:id_solicitado>',endpoints_workers.workers_id),
    path('rest/version/1/workers/<int:id_solicitado>/requests', endpoints_requests.solicitudTrabajador),
    path('rest/version/1/workers/<int:worker_id>/comments', endpoints_feedback.create_comment),
    path('rest/version/1/workers/<int:worker_id>/requests/<int:request_id>', endpoints_feedback.accept_request)
]
