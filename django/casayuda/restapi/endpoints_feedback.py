from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
import smtplib, ssl
from email.mime.text import MIMEText
from email import encoders
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from .models import Tuser, Trequest, Tcomment
import json

@csrf_exempt
def create_comment(request, worker_id):
  if request.method != 'POST':
    return JsonResponse(status=405, data={})

  try:
    token = request.headers['token']
  except KeyError:
    return JsonResponse(status=401, data={})

  try:
    user = Tuser.objects.get(active_session_token = token)
  except Tuser.DoesNotExist:
    return JsonResponse(status=403, data={})

  try:
    worker = Tuser.objects.get(id = worker_id)
  except Tuser.DoesNotExist:
    return JsonResponse(status=404, data={})

  json_body = json.loads(request.body)
  new_comment = Tcomment()
  new_comment.author_user = user
  new_comment.worker_user = worker
  new_comment.message = json_body.get("comment")
  new_comment.rating = json_body["rating"] # Break if rating is not present
  new_comment.save()

  return JsonResponse(status=201, data={})

@csrf_exempt
def accept_request(request, worker_id, request_id):
  if request.method != 'POST':
    return JsonResponse(status=405, data={})

  try:
    token = request.headers['token']
  except KeyError:
    return JsonResponse(status=401, data={})

  try:
    user = Tuser.objects.get(active_session_token = token)
  except Tuser.DoesNotExist:
    return JsonResponse(status=403, data={})

  try:
    request = Trequest.objects.get(id = request_id)
  except Trequest.DoesNotExist:
    return JsonResponse(status=404, data={})

  if user.id != worker_id or request.worker_user.id != worker_id:
    return JsonResponse(status=403, data={"msg": "Inconsistent token data and user id; or request id"})

  request.is_accepted = True
  request.save()

  sender_email = "t2notifications_no_reply@fpcoruna.afundacion.org"
  receiver_email = request.requester_user.email
  password = "Password"
  subject = "Casayuda - Solicitud confirmada"
  salto = "\n"
  body = """\
  ID : """+str(request.id)+salto+"""

  El trabajador ha confirmado la solicitud."""
  # Create a multipart message and set headers
  message = MIMEMultipart()
  message["From"] = sender_email
  message["To"] = receiver_email
  message["Subject"] = subject
  # Add body to email
  message.attach(MIMEText(body, "plain"))
  text = message.as_string()
  with smtplib.SMTP("smtp.gmail.com") as server:
    server.connect("smtp.gmail.com",587)
    server.ehlo()
    server.starttls()
    server.ehlo()
    server.login(sender_email, password)
    server.sendmail(sender_email, receiver_email, text)

  return JsonResponse(status=201, data={})

