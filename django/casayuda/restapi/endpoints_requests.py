from django.http import HttpResponse, JsonResponse
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.utils.datastructures import MultiValueDictKeyError

from .models import Tuser, Trequest, Tcomment
import json

@csrf_exempt
def solicitudTrabajador(request, id_solicitado):
  # Recogemos el token de las cabeceras y lanzamos un 401 si hay problemas
  try:
    token = request.headers['token']
  except KeyError:
    return JsonResponse(status=401, data={})
  # Mediante dicho token, recuperamos al usuario correspondiente y damos 403 si no existe (token no valido)
  try:
    usuario = Tuser.objects.get(active_session_token = token)
  except Tuser.DoesNotExist:
    return JsonResponse(status=403, data={})

  if request.method == 'POST':
    # Controlamos que el trabajador exista
    try:
      trabajador = Tuser.objects.get(id = id_solicitado)
    except Tuser.DoesNotExist:
      return JsonResponse(status=404, data={})

    if usuario.id == id_solicitado:
      return JsonResponse(status=400, data={"msg": "Error. Se ha intentado guardar una request de un usuario hacia si mismo."})

    json_peticion = json.loads(request.body)
    peticion = Trequest()
    peticion.datetime = json_peticion['requested_datetime']
    peticion.message = json_peticion['message']
    peticion.worker_user = trabajador
    peticion.requester_user = usuario
    peticion.is_accepted = False
    peticion.save()
    return JsonResponse(status=201, data={'status':'ok'})
  elif request.method == 'GET':
    # Comprobamos que el ID especificado en el path-param de la peticion es el idoneo
    if usuario.id != id_solicitado:
      return JsonResponse(status=403, data={})

    lista = []
    for peticion in Trequest.objects.filter(worker_user = usuario).order_by("datetime"):
      objeto = {}
      objeto['request_id'] = peticion.id
      objeto['hirer_name'] = peticion.requester_user.name
      objeto['requested_datetime'] = peticion.datetime
      objeto['message'] = peticion.message
      objeto['is_accepted'] = peticion.is_accepted == 1
      lista.append(objeto)
    return JsonResponse(status=200, data=lista, safe=False)
  else:
    return JsonResponse(status=405, data={})


