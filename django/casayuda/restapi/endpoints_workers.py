

from distutils.log import ERROR
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from .models  import Tuser, Tcomment,Trequest
from django.forms.models import model_to_dict
import math
import json

@csrf_exempt
def workers(request):
    if request.method == 'GET':
        try:
            latitude =  request.GET['latitude']
            longitude =  request.GET['longitude']
            radius1 =  request.GET['radius']
        except KeyError:
            return JsonResponse(status=400,data={'error':'La petición ha fallado porque falta alguno de los parámetros o es inválido'})
            
        try:
            token = request.headers['token']    
        except KeyError:
            return JsonResponse(status=401,data={'error':'La petición ha fallado porque no se ha enviado el token de autenticación en las cabeceras'})  
        
        try:
            user_id = Tuser.objects.get(active_session_token = token)
        except Tuser.DoesNotExist:    
            return JsonResponse(status=403,data={'error':'La petición ha fallado porque el token de autenticación no es válido'})

        users = Tuser.objects.all()

        latitud_param =  request.GET['latitude']
        longitude_param =  request.GET['longitude']
        radius =  request.GET['radius']
        respuesta = []
        radius_earth = 6373.0

        for user in users:
            if(user.jobs is not None):
                lat1 = math.radians(user.latitude)
                lon1 = math.radians(user.longitude)
                lat2 = math.radians(float(latitud_param))
                lon2 = math.radians(float(longitude_param))
                
                dlon = lon2 - lon1
                dlat = lat2 - lat1

                a = math.sin(dlat / 2)**2 + math.cos(lat1) * math.cos(lat2) * math.sin(dlon / 2)**2
                c = 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a))

                distance = radius_earth * c
                print(distance)

                if distance > float(radius):
                    continue  
                diccionario = {}
                diccionario['user_id'] = user.id
                diccionario['name'] = user.name
                diccionario['surname'] = user.surname
                diccionario['jobs'] = user.jobs.split(',')
                diccionario['price_per_hour'] = user.price_per_hour
                respuesta.append(diccionario)
        return JsonResponse(respuesta, safe=False, status=200)
    else:
        return JsonResponse(status=405,data={'error':405})

def workers_id(request, id_solicitado):
    if request.method == 'GET':
        hired_by_user_id = request.GET['hired_by_user_id']
        try:
            token = request.headers['token']    
        except KeyError:
            return JsonResponse(status=401,data={'error':'La petición ha fallado porque no se ha enviado el token de autenticación en las cabeceras'})  
        try:
            user_id = Tuser.objects.get(id=hired_by_user_id,active_session_token = token )
        except Tuser.DoesNotExist:
            return JsonResponse(status=403,data={'error':'La petición ha fallado porque el token de sesión no es válido o no se corresponde con el ID de usuario hired_by_user_id'})  
        try:
            user_id = Tuser.objects.get(id = id_solicitado )
        except Tuser.DoesNotExist:
            return JsonResponse(status=404,data={'error':'La petición ha fallado porque no existe un trabajador con el ID especificado'}) 
        
        work = Tuser.objects.get(id = id_solicitado)
        comentarios = Tcomment.objects.filter(worker_user_id = id_solicitado)
        solicitudes = Trequest.objects.filter(worker_user_id = id_solicitado).filter(requester_user_id = hired_by_user_id )

        if(work.jobs is not None):
            diccionario = {}
            diccionario['user_id'] = work.id
            diccionario['name'] = work.name
            diccionario['surname'] = work.surname
            diccionario['jobs'] = work.jobs.split(',')
            diccionario['price_per_hour'] = work.price_per_hour
        else:
            return JsonResponse(data={'error':'Este usuario no es un trabajador'})
        lista_solicitudes = []
        for solicitud in solicitudes:
            dic_solicitud = {}
            dic_solicitud['datetime'] = solicitud.datetime
            dic_solicitud['hirer_id'] = solicitud.requester_user_id
            if solicitud.is_accepted == 0:
                dic_solicitud['is_accepted'] = False
            elif solicitud.is_accepted == 1: 
                dic_solicitud['is_accepted'] = True
            lista_solicitudes.append(dic_solicitud)
        diccionario['hired_before'] = lista_solicitudes

        lista_comentarios = []
        for comentario in comentarios:
            dic_comentario = {}
            user = Tuser.objects.get(id = comentario.author_user_id)
            dic_comentario['author_name'] = user.name
            dic_comentario['rating'] = comentario.rating
            dic_comentario['comment'] = comentario.message
            lista_comentarios.append(dic_comentario)
        diccionario['comments'] = lista_comentarios

        return JsonResponse(status=200,safe=False,data=diccionario)
    else:
        return JsonResponse(status=405,data={'error':'405'})
















